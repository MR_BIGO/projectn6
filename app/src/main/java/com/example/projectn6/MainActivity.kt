package com.example.projectn6

import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var sharedPreferences: SharedPreferences

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        read()
    }

    private fun init(){
        sharedPreferences = getSharedPreferences("data", MODE_PRIVATE)
    }


    fun save(view: View) {
        if (FirstNameEditText.text.toString().isNotEmpty()&&
            LastNameEditText.text.toString().isNotEmpty()&&
            EmailEditText.text.toString().isNotEmpty()&&
            AddressEditText.text.toString().isNotEmpty()&&
            AgeEditText.text.toString().isNotEmpty()) {

                val firstName = FirstNameEditText.text.toString()
                val email = EmailEditText.text.toString()
                val lastName = LastNameEditText.text.toString()
                val age = AgeEditText.text.toString().toInt()
                val address = AddressEditText.text.toString()




                val edit = sharedPreferences.edit()
                edit.putString("firstName", firstName)
                edit.putString("lastName", lastName)
                edit.putString("email", email)
                edit.putString("address", address)
                edit.putInt("age", age)
                edit.apply()
        }
        else{
            Toast.makeText(this, "Fill all of the fields", Toast.LENGTH_SHORT).show()
        }

    }

     private fun read(){
        val firstName = sharedPreferences.getString("firstName", "")
        val lastName = sharedPreferences.getString("lastName", "")
        val email = sharedPreferences.getString("email", "")
        val address = sharedPreferences.getString("address", "")
        val age = sharedPreferences.getInt("age", 0)

        FirstNameEditText.setText(firstName)
        LastNameEditText.setText(lastName)
        EmailEditText.setText(email)
        AddressEditText.setText(address)
        AgeEditText.setText(age.toString())

    }


}